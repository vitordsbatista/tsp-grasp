#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Vitor Batista
vitordsbatista@gmail.com
Python Version: 2.7
"""

import matplotlib.pyplot as plt
import pandas as pd
import random as rd
import networkx as nx

def eval(sol, inst):
    """
    Função de avaliação
    """
    eval_sum = 0
    #Varre o vetor
    for i, j in zip(sol[:-1], sol[1:]):
        if inst.has_key((i, j)):
            eval_sum += inst[(i, j)]
        elif inst.has_key((j, i)): 
            eval_sum += inst[(j, i)]
        else:
            return False
    return eval_sum
def neig(sol, n=1):
    """
    Função de vizinhança
    """
    #Executa n vezes
    k = range(len(sol))
    for i in range(n):
        #Selciona 't' indices de elementos da solução
        t1, t2 = rd.sample(k, 2)
        #Muda  eles de lugar na solução
        sol[t1], sol[t2] = sol[t2], sol[t1]
    return sol

#Lê os dados do cv
b58 = dict()
with open('brazil58.txt', 'r') as f:
    tmp = f.readlines()
    for i in tmp:
        t, f, v = map(int, i.split(' '))
        b58[(t, f)] = v

#Gera uma solução inicial aleatória
sol = range(58)
fit = eval(sol, b58)

#Melhor solução
best_sol = sol
best_fit = fit

#Melhores soluções para o plot
bests = [best_fit]
print 'First solution', fit
n = 1
#GRASP
for i in range(100):
    #Cria uma solução
    old_sol = sol
    # Seleciona um elemento aleatório pra ser o primeiro
    sol = [old_sol.pop(rd.randint(0, len(old_sol)-1))]
    while old_sol:
        #Último elemento da solução
        last = sol[-1]
        # Adiciona os valores de cada trecho
        values = []
        for j in old_sol:
            if b58.has_key((last, j)):
                val = b58[(last, j)]
            else:
                val = b58[(j, last)]
            values.append((j, val)) 
        # Ordena
        values = sorted(values, key=lambda (k, v): v) 
        # Seleciona um elemento aleatório dos n melhores elementos
        choosed = rd.choice(values[:n])[0]
        # Adiciona este valor a solução e remove da solução antiga
        sol.append(choosed)
        old_sol.remove(choosed)
    # Avalia a solução gerada
    fit = eval(sol, b58)
    #Compara a solução gerada com a melhor solução
    if fit < best_fit:
        print 'Solution changed to', fit, 'at iteration:', i
        best_sol = sol
        best_fit = fit
    #Busca Local (HillClimb)
    # Função de vizinhança na solução
    for j in range(5):
        sol_tmp = neig(sol, 10)
        fit_tmp = eval(sol_tmp, b58)
        if fit_tmp < best_fit:
            print 'Solution changed to', fit, 'at iteration:', i
            best_sol = sol_tmp
            best_fit = fit_tmp
    #print sol
    print best_sol
    bests.append(best_fit)

#Plot
plt.plot(bests)
#plt.show()

#Plot da melhor solução
g = nx.Graph()
g.add_nodes_from(range(58))
g.add_edges_from(b58)
for i, j in b58:
    #g[i][j]['weight'] = b58[(i, j)]
    g[i][j]['color'] = 'c'
for i, j in zip(best_sol[:-1], best_sol[1:]):
    g[i][j]['color'] = 'r'

edges = g.edges()
colors = [g[u][v]['color'] for u,v in edges]
#nx.draw(g, with_labels=True, edge_color=colors)
nx.draw(g, with_labels=True, edge_color=colors)
#plt.show()
